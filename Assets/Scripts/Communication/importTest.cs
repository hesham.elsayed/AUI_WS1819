﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Receive
{
public class importTest : MonoBehaviour {

		int toggleTimer = 30;
		bool ledState = false;

		public bool startMotor = false;
		public string direction = "Up";

	// Use this for initialization
		void Start () {
			StartCoroutine (recieveLoop ());
		}
		
		// Update is called once per frame
		void Update () {

		}

		IEnumerator recieveLoop(){
			var receive = new Receive {RunLoop = true};
			receive.Setup();

			int i = 0;
			while (receive.RunLoop) {
				
				receive.setMotor (direction,startMotor);
				
				if(i >= toggleTimer){
					ledState = !ledState;
					i = 0;
				}

				i++;
				receive.toggleLed (ledState);
				yield return null;
			} 
			receive.Exit();
		}
	}
}
