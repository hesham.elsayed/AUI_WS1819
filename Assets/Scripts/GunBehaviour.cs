﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class GunBehaviour : MonoBehaviour {

    public SteamVR_Input_Sources handType;
    public SteamVR_Action_Boolean triggertAction;
    public SteamVR_Behaviour_Pose controllerPose;

    public GameObject laserPrefab;
    private GameObject laser;

    public GameObject aimCirclePrefab;
    private GameObject aimCircle;
    public Vector3 aimCircleOffset;
    private Transform aimCircleTransform;

    private Transform laserTransform;
    private Vector3 hitPoint;
    //private Vector3 fullLaserScale;
    public Material activeLaser;
    private Material inactiveLaser;

    private bool laserActive = false;

    // Use this for initialization
    void Start () {
        laser = Instantiate(laserPrefab);
        laserTransform = laser.transform;
        aimCircle = Instantiate(aimCirclePrefab);
        aimCircleTransform = aimCircle.transform;
        //aimCircleTransform.Rotate(0,90,0);

        //fullLaserScale = new Vector3(0.005f,0.005f,1f);
        inactiveLaser = laser.GetComponent<Renderer>().material;

    }

	


	void Update () {
	    RaycastHit hit;
	    Physics.Raycast(controllerPose.transform.position, transform.forward, out hit, 100);
	    hitPoint = hit.point;
	    aimCircle.SetActive(true);
	    aimCircleTransform.position = hitPoint + aimCircleOffset;
	    ShowLaser(hit);

	    if (TriggerThisFrame() || laserActive)
	    {
	        if (!laserActive)
	        {
	            StartCoroutine(ActivateLaser());
	        }


	        if (hit.collider != null && hit.collider.gameObject.tag.Equals("Cube") && laserActive)
	            {
	                hit.collider.gameObject.GetComponent<CubeBehaviour>().StartRotation();
	                //Debug.Log("Hit Target: " + hit.collider.gameObject.ToString() + "At: " + Time.time);
	            }
	            else
	            {
	                //Debug.Log("Hit Other: " + hit.collider.gameObject.ToString() + "At: " + Time.time);
	            }


	    }


	}

    public bool TriggerThisFrame()
    {
        return triggertAction.GetStateDown(handType);
    }

    private void ShowLaser(RaycastHit hit)
    {
        //laser.SetActive(true);
        laserTransform.position = Vector3.Lerp(controllerPose.transform.position, hitPoint, 0.5f);
        laserTransform.LookAt(hitPoint);
        laserTransform.localScale = new Vector3(laserTransform.localScale.x,laserTransform.localScale.y,hit.distance);
    }

    private IEnumerator ActivateLaser()
    {
        laserActive = true;
        //Debug.Log("Started corutine laser active at: " + Time.time + laserActive);
        
        laserTransform.localScale = new Vector3(0.005f, 0.005f, laserTransform.localScale.z);
        laser.GetComponent<Renderer>().material = activeLaser;
        for (int i = 0; i < 30; i++)
        {
            yield return null;
        }
        DeactivateLaser();
    }

    private void DeactivateLaser()
    {
        laserActive = false;
        laserTransform.localScale = new Vector3(0.001f, 0.001f, laserTransform.localScale.z);
        laser.GetComponent<Renderer>().material = inactiveLaser;
        //Debug.Log("laser deactivated at: " + Time.time);

    }

    public Vector3 getAimPosition()
    {
        return aimCircleTransform.transform.position;
    }

    public Transform getAimTransform()
    {
        return aimCircleTransform.transform;
    }

}
