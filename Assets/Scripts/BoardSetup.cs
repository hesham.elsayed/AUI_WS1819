﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Receive
{

public class BoardSetup : MonoBehaviour {

	public GameObject cubePrefab;
	public GameObject redCrossPrefab;
	public GameObject blueCrossPrefab;
	public Text timerText;
	public Text escMessage;
    public bool shwoVisualCue = true;

    public Vector3 startPosition;

    public GameObject rightHandGun;

    float rotationSpeed = 5f;

    Receive messenger;
    public  GameObject vmObject;
    VirbationManager vm;


    float cubeDistance = 2.2f;
	float crossDistance = 7.2f;
	float lenght = 4f;
	float height = 4f;

	int maxRed;
	int maxBlue;

	int foundRed = 0;
	int foundBlue = 0;

	private List<GameObject> cubeList;

	GameObject currentRed;
	GameObject currentBlue;

	GameObject redCross;
	GameObject blueCross;

	DirectionIndicator[] redArrows;
	DirectionIndicator[] blueArrows;

	bool timerActive = true;

	// Use this for initialization
	void Start () {
		maxRed = 16;//(int) Mathf.Floor ((lenght * height) / 2f);
		maxBlue = 0;//(int) Mathf.Ceil ((lenght * height) / 2f);

			messenger = new Receive {RunLoop = true, protID = 3};
			messenger.Setup();

		cubeList = new List<GameObject> ();

         

		//Vector3 startPosition = new Vector3(-3.5f,-2.5f,0f);
		for (int i = 0; i < lenght; i++) {
			for (int j = 0; j < height; j++) {
				Vector3 spawnPosition = startPosition + new Vector3(i * cubeDistance,j * cubeDistance,0);
				GameObject currentCube = Instantiate (cubePrefab, spawnPosition, Quaternion.identity);
				currentCube.transform.RotateAround (currentCube.transform.position, Vector3.down, 180f);
				cubeList.Add (currentCube);

			}
		}

		SetRandomRed ();
		//SetRandomBlue ();

		redCross = Instantiate (redCrossPrefab, startPosition + new Vector3( 2*crossDistance,1,0), Quaternion.identity);
		redArrows = redCross.GetComponentsInChildren<DirectionIndicator> ();

		//blueCross = Instantiate (blueCrossPrefab, new Vector3(-crossDistance,1,-10), Quaternion.identity);
		//blueArrows = blueCross.GetComponentsInChildren<DirectionIndicator> ();
	
		
	    //setCrossVisibility(shwoVisualCue);

        vm = vmObject.GetComponent<VirbationManager>();
            vm.arrows = redArrows;
            vm.messenger = messenger;
            SetupArrows();
            




        }

	void SetRandomRed(){
		if (cubeList.Count > 0 && foundRed < maxRed) {
			int randomIndex = Random.Range (0, cubeList.Count);

			cubeList [randomIndex].GetComponent<CubeBehaviour> ().ToggleToRed ();
			currentRed = cubeList [randomIndex];
			cubeList.RemoveAt (randomIndex);
		}
	}

	void SetRandomBlue(){
		if (cubeList.Count > 0 && foundBlue < maxBlue) {
			int randomIndex = Random.Range (0, cubeList.Count);

			cubeList [randomIndex].GetComponent<CubeBehaviour> ().ToggleToBlue ();
			currentBlue = cubeList [randomIndex];
			cubeList.RemoveAt (randomIndex);
		}
	}

	void SetupArrows(){
		for (int i = 0; i < 4; i++) {
			if (foundRed == maxRed) {
				redArrows [i].currentlyActive = false;
                    vm.gameEnded = true;
			} else {
                    vm.TargetPostion = currentRed.transform;
                    vm.pointerPosition = rightHandGun.GetComponent<GunBehaviour>().getAimTransform();
                    vm.initialized = true;
                redArrows [i].target = currentRed;
			    redArrows[i].messenger = messenger;
			    redArrows[i].crossCenter = redCross.transform.position;
                }
                /*
                if (foundBlue == maxBlue) {
                    blueArrows [i].currentlyActive = false;
                } else {
                    blueArrows [i].target = currentBlue;
                }*/
		    redArrows[i].pointerPosition = rightHandGun.GetComponent<GunBehaviour>().getAimPosition();
		    redArrows[i].GetComponent<MeshRenderer>().enabled = shwoVisualCue;


		}
	}

    void setCrossVisibility(bool b)
    {
        MeshRenderer[] mr = redCross.GetComponentsInChildren<MeshRenderer>();
        for(int i = 0;i<mr.Length;i++)
        {
            mr[i].enabled = b;

        }
        }
	
	// Update is called once per frame
	void Update () {
	    SetupArrows();
            if (currentRed.GetComponent<CubeBehaviour> ().uncovered && foundRed < maxRed) {
			foundRed++;
			SetRandomRed ();
			SetupArrows ();
		}/*
		if (currentBlue.GetComponent<CubeBehaviour> ().uncovered && foundBlue < maxBlue) {
			foundBlue++;
			SetRandomBlue ();
			SetupArrows();
		}*/
		if (timerActive) {
			if (!(foundRed == maxRed && foundBlue == maxBlue)) {
				float passedTime = Time.time;

				int minutes = (int)Mathf.Floor (passedTime / 60);
				int seconds = (int)Mathf.Floor (passedTime % 60);

				string secondsString = seconds.ToString ();
				if (seconds < 10) {
					secondsString = "0" + seconds;
				} 
				timerText.text = minutes + ":" + secondsString;
			} else {
				timerActive = false;
				timerText.text = "Final Time: " + timerText.text;
				escMessage.text = "Press Esc to quit.";
			}
		}

		if (Input.GetKeyDown(KeyCode.Escape)) {
			Application.Quit ();
		}

	    float horizontalInput = Input.GetAxis("Horizontal");

	    if (horizontalInput != 0 && redArrows[0].target != null)
	    {
	        redCross.transform.RotateAround(redCross.transform.position, Vector3.forward, rotationSpeed * -horizontalInput);
                

        }

        redCross.transform.rotation = Quaternion.Euler(0,0,rightHandGun.transform.rotation.eulerAngles.z);
            vm.rotationAngle = -redCross.transform.rotation.eulerAngles.z;



        }
}
}
