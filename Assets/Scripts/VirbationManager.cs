﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Receive { 

public class VirbationManager : MonoBehaviour
{

        public Transform TargetPostion;
        Transform RotatedTargetPostion;
        public Transform pointerPosition;
        public bool initialized = false;
        public bool gameEnded = false;
        public Receive messenger;

        public float rotationAngle;

        float minimumTreshhold = 0.8f;

        bool rightXPosition;
        bool rightYPosition;

        public DirectionIndicator[] arrows;
    // Start is called before the first frame update
    void Start()
    {
            GameObject go = new GameObject();
            RotatedTargetPostion = go.transform;
    }

    // Update is called once per frame
    void Update()
    {

            RotatedTargetPostion.position = TargetPostion.position;
            RotatedTargetPostion.RotateAround(pointerPosition.position, Vector3.forward, rotationAngle);

             rightXPosition = (Mathf.Abs((RotatedTargetPostion.position.x - pointerPosition.position.x)) <= minimumTreshhold);
             rightYPosition = (Mathf.Abs((RotatedTargetPostion.position.y - pointerPosition.position.y)) <= minimumTreshhold);

            if (initialized && TargetPostion != null && pointerPosition != null) { 
            //Up
             handleMotor(arrows[0], "Up", checkDirectionPositive(RotatedTargetPostion.position.y, pointerPosition.position.y));

            //Down
             handleMotor(arrows[1], "Down", checkDirectionNegative(RotatedTargetPostion.position.y, pointerPosition.position.y));

             //Right
             handleMotor(arrows[3], "Right", checkDirectionPositive(RotatedTargetPostion.position.x, pointerPosition.position.x));

             //Left
             handleMotor(arrows[2], "Left", checkDirectionNegative(RotatedTargetPostion.position.x, pointerPosition.position.x));
            }

        }


        bool checkDirectionNegative(float targetCoordinate, float pointerCoordinate) {
            return (targetCoordinate - pointerCoordinate <= -minimumTreshhold || (rightXPosition && rightYPosition)) && !gameEnded ;
        }

        bool checkDirectionPositive(float targetCoordinate, float pointerCoordinate)
        {
            return (targetCoordinate - pointerCoordinate >= minimumTreshhold || (rightXPosition && rightYPosition)) && !gameEnded;
        }


        void handleMotor(DirectionIndicator arrow, string direction, bool active) {
            arrow.setFlash(active);
            messenger.setMotor(direction, active);
        }

    }
}
