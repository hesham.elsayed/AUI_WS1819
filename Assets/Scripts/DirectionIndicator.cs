﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace Receive
{
public class DirectionIndicator : MonoBehaviour {

	public Material darkColor;
	public Material brightColor;

	public string direction;
	public Receive messenger;

	public GameObject target;
    public Vector3 pointerPosition;

	private Camera cam;

	float transitionSpeed = 0.001f;
	float lerpFactor = 0;
	int decreaseOrIncrease = 1;

	float maxFlashRate = 0.3f;
	float minFlashRate = 0.05f;

	float distanceMultiplyer = 0.2f;

	public int xAxis = 0;
	public int yAxis = 1;

	float minimumTreshhold = 0.8f;
	public bool currentlyActive = true;

	Material thisMaterial;

    int frameCounter = 0;
    int vibrationIntervall = 12;
    public int frameOffset;

    bool onRightTarget = false;

    float positionTreshhold = 0.7f;

    public Vector3 crossCenter;

    public float flashRate = -1;


        // Use this for initialization
        void Start () {
		thisMaterial = GetComponent<Renderer> ().material;
		cam = Camera.main;
		
	}
		
	
	void Update () {

	    frameCounter++;
	    if (frameCounter >= frameOffset + vibrationIntervall * 2)
	    {
	        frameCounter = 0;
	    }

	    /*if (transform.position.x < -positionTreshhold + crossCenter.x)
	    {
	        xAxis = -1;
	    }
	    else if (transform.position.x > positionTreshhold + crossCenter.x)
	    {
	        xAxis = 1;
	    }
	    else
	    {
	        xAxis = 0;
	    }

	    if (transform.position.y < -positionTreshhold + crossCenter.y)
	    {
	        yAxis = -1;
	    }
	    else if (transform.position.y > positionTreshhold + crossCenter.y)
	    {
	        yAxis = 1;
	    }
	    else
	    {
	        yAxis = 0;
	    }*/
            if (target != null && currentlyActive && getFlashRate() >= 0) {

			transitionSpeed = Mathf.Min (Mathf.Max (getFlashRate (), minFlashRate),maxFlashRate);
			lerpFactor += decreaseOrIncrease * transitionSpeed;
		    //setMotorActive();


            if (lerpFactor > 1f) {
				decreaseOrIncrease = -1;
			} else if (lerpFactor < 0f) {
				decreaseOrIncrease = 1;
			}
		
		} else {
			lerpFactor = 0f;
				//messenger.setMotor (direction, false);
		}
		thisMaterial.Lerp (darkColor, brightColor, lerpFactor);

	}

	float getFlashRate(){
		/*float result;
		//Vector3 pointerCoordinates = cam.ScreenToWorldPoint(new Vector3( Input.mousePosition.x,Input.mousePosition.y,0));
	    Vector3 pointerCoordinates = pointerPosition;

            bool rightXPosition = ( Mathf.Abs ((target.transform.position.x - pointerCoordinates.x)) <= minimumTreshhold);
		bool rightYPosition = ( Mathf.Abs((target.transform.position.y - pointerCoordinates.y)) <= minimumTreshhold);

		if (rightXPosition && rightYPosition) {
			result = maxFlashRate;
		    onRightTarget = true;
            }
            else if((xAxis != 0 && rightXPosition) || (yAxis != 0 && rightYPosition)){
			result = -1;
		    onRightTarget = false;
            }
		else {
		result = distanceMultiplyer * (1/ ((xAxis*( target.transform.position.x - pointerCoordinates.x )) + 
			yAxis*( target.transform.position.y - pointerCoordinates.y )));
		    onRightTarget = false;
            }
            */
		return flashRate;

	}

        public void setFlash(bool b) {
            if (b) {
                flashRate = maxFlashRate * 0.3f;
            }
            else
            {
                flashRate = -1;
            }
        }


    void setMotorActive()
    {
        if (frameCounter >= frameOffset + vibrationIntervall || onRightTarget)
        {
            messenger.setMotor(direction, true);
        }
        else
        {
            messenger.setMotor(direction, false);
        }
    }
    }
}
