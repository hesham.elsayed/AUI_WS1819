﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeBehaviour : MonoBehaviour {

	public int framesPerRotation = 20;
	public int waitingFrames = 30;
	public string currentSymbol = "black";
	static bool currentlyMooving = false;
	public bool uncovered =  false;


	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnMouseDown(){
		StartRotation();
	}

	public void StartRotation(){
		if (!(currentlyMooving || uncovered)) {
			StartCoroutine (RotateOnClick ());
		}
	}

	IEnumerator RotateOnClick(){
		currentlyMooving = true;
		for(int i = 0; i < framesPerRotation; i++){
		transform.RotateAround (transform.position, Vector3.down, (float)180 / framesPerRotation);
			yield return null;
		}

		if (currentSymbol != "black") {
			uncovered = true;
		} else {
			for(int i = 0; i < waitingFrames; i++){
				yield return null;
			}

			for(int i = 0; i < framesPerRotation; i++){
				transform.RotateAround (transform.position, Vector3.down, (float)180 / framesPerRotation);
				yield return null;
			}
		}
		currentlyMooving = false;

	}

	public void ToggleToRed(){
		currentSymbol = "red";
		GetComponentsInChildren<MeshRenderer> () [1].enabled = true;
		GetComponentsInChildren<MeshRenderer> () [2].enabled = false;
		GetComponentsInChildren<MeshRenderer> () [3].enabled = false;
		GetComponentsInChildren<MeshRenderer> () [4].enabled = false;
	}

	public void ToggleToBlue(){
		currentSymbol = "blue";
		GetComponentsInChildren<MeshRenderer> () [2].enabled = true;
		GetComponentsInChildren<MeshRenderer> () [1].enabled = false;
		GetComponentsInChildren<MeshRenderer> () [3].enabled = false;
		GetComponentsInChildren<MeshRenderer> () [4].enabled = false;
	}
}
