// *** Receive ***

// This 1st example will make the PC toggle the integrated led on the arduino board. 
// It demonstrates how to:
// - Define commands
// - Set up a serial connection
// - Receive a command with a parameter from the PC

#include <CmdMessenger.h>  // CmdMessenger

SYSTEM_MODE(SEMI_AUTOMATIC);

// Blinking led variables 
bool ledState                   = 0;   // Current state of Led
const int kBlinkLed             = D7;  // Pin of internal Led

const int motorUp             = A0;
const int motorDown           = A4;
const int motorLeft           = A6;
const int motorRight          = A2;

// Attach a new CmdMessenger object to the default Serial port
CmdMessenger cmdMessenger = CmdMessenger(Serial);

// We can define up to a default of 50 cmds total, including both directions (send + receive)
// and including also the first 4 default command codes for the generic error handling.
// If you run out of message slots, then just increase the value of MAXCALLBACKS in CmdMessenger.h

 // This is the list of recognized commands. These can be commands that can either be sent or received. 
 // In order to receive, attach a callback function to these events
 // 
 // Note that commands work both directions:
 // - All commands can be sent
 // - Commands that have callbacks attached can be received
 // 
 // This means that both sides should have an identical command list:
 // both sides can either send it or receive it (or even both)    

// Commands
enum
{
  kSetLed, // Command to request led to be set in specific state
  kSetMotor,
};

// Callbacks define on which received commands we take action 
void attachCommandCallbacks()
{
  cmdMessenger.attach(kSetLed, OnSetLed);
  cmdMessenger.attach(kSetMotor, OnSetMotor);
}

// Callback function that sets led on or off
void OnSetLed()
{
  // Read led state argument, interpret string as boolean
  ledState = cmdMessenger.readBoolArg();
  // Set led
  digitalWrite(kBlinkLed, ledState?HIGH:LOW);
}


void OnSetMotor()
{
  String motorDirection = cmdMessenger.readStringArg();
  bool state = cmdMessenger.readBoolArg();
  int intensity = LOW;
  if(!state){
    intensity = HIGH;
    }
  if(motorDirection == "Up"){
    digitalWrite(motorUp, intensity);
    }
   else if(motorDirection == "Down"){
    digitalWrite(motorDown, intensity);
    }
   else if(motorDirection == "Left"){
    digitalWrite(motorLeft, intensity);
    }
   else if(motorDirection == "Right"){
    digitalWrite(motorRight, intensity);
    }    
}



// Setup function
void setup() 
{
   RGB.control(true);
  pinMode(motorUp,OUTPUT);
  pinMode(motorRight,OUTPUT);
  pinMode(motorDown,OUTPUT);
  pinMode(motorLeft,OUTPUT);


  digitalWrite(motorUp, HIGH);
  digitalWrite(motorRight, HIGH);
  digitalWrite(motorDown, HIGH);
  digitalWrite(motorLeft, HIGH);
  // Listen on serial connection for messages from the PC
  // 115200 is the max speed on Arduino Uno, Mega, with AT8u2 USB
  // Use 57600 for the Arduino Duemilanove and others with FTDI Serial
  Serial.begin(115200); 

  // Adds newline to every command
  cmdMessenger.printLfCr();   

  // Attach my application's user-defined callback methods
  attachCommandCallbacks();

  // set pin for blink LED
  pinMode(kBlinkLed, OUTPUT);
  
}

// Loop function
void loop() 
{
  // Process incoming serial data, and perform callbacks
  cmdMessenger.feedinSerialData();
}
